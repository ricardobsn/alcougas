package com.example.ricardoneves.gasolinaoualcool;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.widget.Toast;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private TextView usuNome;
    public SQLiteDatabase baseDadosUsu;
    private TextView resultado;
    private Button verificar;
    private EditText precoAlcool;
    private EditText precoGasolina;
    private Button historico;
    private List<Info> historicoList = new ArrayList<>();
    private Info info;
    private String aux;
    float ultA = 0f;
    float ultG = 0f;
    private ArrayAdapter<String> arrayAdap;
    private long lastUpdate = 0;
    private SegundaActivity aux3;
    private DataBase bandados;
    public SQLiteDatabase baseDadosComb;
    private Button sobre;
    private static final String ARQUIVO_PREFERENCIA = "arquivoPreferencia";
    private int idUsu;
    private String loginUsu;
    private Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lastUpdate = System.currentTimeMillis();
        loginUsu= "";
        usuNome = (TextView) findViewById(R.id.nomeUsuarioId);
        resultado = (TextView) findViewById(R.id.resultadoID);
        verificar = (Button) findViewById(R.id.verificarID);
        precoAlcool = (EditText) findViewById(R.id.alcoolID);
        precoGasolina = (EditText) findViewById(R.id.gasolinaID);
        historico = (Button) findViewById(R.id.historicoId);
        sobre= (Button) findViewById(R.id.sobreId);
        verificar.setEnabled(false);
        logout = (Button) findViewById(R.id.logoutId);

        usuNome.setText("OLÁ" + " "+ getlogin()+"!");


        try {
            bandados = new DataBase(this);
            baseDadosComb = bandados.getWritableDatabase();

            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setMessage("Banco criado com sucesso");
            build.setNeutralButton("OK", null);
           // build.show();
        } catch (SQLException ex) {
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setMessage("Erro ao criar o banco:" + ex.getMessage());
            build.setNeutralButton("OK", null);
            build.show();
        }

        precoAlcool.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                verificarBotao();
            }
        });

        precoGasolina.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                verificarBotao();
            }
        });


        verificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alcool = precoAlcool.getText().toString();
                String gasolina = precoGasolina.getText().toString();

                if (alcool == null || alcool.isEmpty() || gasolina == null || gasolina.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Preencha os Campos Obrigatórios",
                            Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        float gasolinaAux = Float.parseFloat(gasolina);
                        float alcoolAux = Float.parseFloat(alcool);
                        float resultadoFinal = gasolinaAux * 0.7f;

                        if (resultadoFinal == 0.0f) {
                            resultado.setText("Abasteça com Gasolina");
                        } else if (resultadoFinal > alcoolAux) {
                            resultado.setText("Abasteça com Álcool");
                        } else if (resultadoFinal < alcoolAux) {
                            resultado.setText("Abasteça com Gasolina");
                        }
                    } catch (NumberFormatException e) {
                        resultado.setText("Insira um Número Válido");
                    } catch (Exception e) {
                        resultado.setText("Ocorreu um erro inesperado.");
                    }
                }

                if (canUpdate(alcool, gasolina)) {
                    criarLog();
                } else {
                    Toast.makeText(getApplicationContext(), "Cálculo já Realizado!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        historico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pesquisarBase();
             //   startActivity(new Intent(MainActivity.this, SegundaActivity.class));
            }
        });

        sobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SobreActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }
        });

    }

    public boolean canUpdate(String a, String g) {
        float auxA = Float.parseFloat(a);
        float auxG = Float.parseFloat(g);

        if ((auxA != ultA) || (auxG != ultG)) {
            ultA = auxA;
            ultG = auxG;

            return true;
        } else {
            return false;
        }
    }

    public void criarLog() {
        Date d1 = new Date();
        aux = resultado.getText().toString();
        info = new Info(d1, aux);
        historicoList.add(info);

        try {
            for (Info info : historicoList) {
                insertContact(info.getData().getTime(), info.getInfo());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean insertContact(long d, String inf) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("data", d);
        contentValues.put("info", inf);
        contentValues.put("trackUsuario", this.getUserId());


        baseDadosComb.insert("LogComb", null, contentValues);
        return true;
    }

    public void verificarBotao() {
        verificar.setEnabled(false);

        if ((precoAlcool.getText().length() > 0) && (precoGasolina.getText().length() > 0))
            verificar.setEnabled(true);
    }

    public int getUserId() {

        SharedPreferences sharedpreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, Context.MODE_PRIVATE);
        if (sharedpreferences.contains("id")){
            idUsu= sharedpreferences.getInt("id",0);
        }
        return idUsu;
    }
    public String getlogin() {

        SharedPreferences sharedpreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, Context.MODE_PRIVATE);
        if (sharedpreferences.contains("login")){
            loginUsu= sharedpreferences.getString("login","");
        }
        return loginUsu;
    }

    public void pesquisarBase() {

        String research = "SELECT * FROM LogComb WHERE trackUsuario = "+ this.getUserId();
        Cursor cursor = baseDadosComb.rawQuery(research, null);

       if(cursor.getCount() <= 0){
            Toast.makeText(getApplicationContext(), "Histórico Vazio!", Toast.LENGTH_SHORT).show();
            cursor.close();

        }
        else{
           startActivity(new Intent(MainActivity.this, SegundaActivity.class));
       }
    }



}





