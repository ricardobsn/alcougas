package com.example.ricardoneves.gasolinaoualcool;

/**
 * Created by ricardo.neves on 18/07/2016.
 */

import android.content.Context;
import android.database.sqlite.*;
import android.util.StringBuilderPrinter;


public class DataBase extends SQLiteOpenHelper{


    public DataBase(Context context) {
        super(context, "LogComb", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(getCreate());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    public static String getCreate(){


        StringBuilder build= new StringBuilder();

        build.append("CREATE TABLE IF NOT EXISTS LogComb ( ");
        build.append("_id            INTEGER             NOT NULL ");
        build.append("PRIMARY KEY AUTOINCREMENT , ");
        build.append("data              DATE, ");
        build.append("info              VARCHAR (20), ");
        build.append("trackUsuario      INTEGER, ");
        build.append("FOREIGN KEY(trackUsuario) REFERENCES Usuario(_id_Usu) ");
        build.append("); ");

        return build.toString();
    }
}
