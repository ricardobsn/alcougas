package com.example.ricardoneves.gasolinaoualcool;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Info implements Serializable {

    private Date data = new Date();
    private String info;

    public Info() {
    }

    public Info(Date dat, String inf){
        setData(dat);
        setInfo(inf);
    }

    public Date getData() {
        return data;
    }

    public String getPrettyData() {
        return new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(data);
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setData(Long miliseconds) {
        this.data = new Date(miliseconds);
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "\n Data: " + data + "\n Info: " + info;
    }
}
