package com.example.ricardoneves.gasolinaoualcool;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;


public class TodoCursorAdapter extends CursorAdapter {

    private Info p = new Info();

    public TodoCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // when the view will be created for first time,
        // we need to tell the adapters, how each item will look
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.itemlist, parent, false);

        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // here we are setting our data
        // that means, take the data from the cursor and put it in views
        p.setData(cursor.getLong(cursor.getColumnIndex(cursor.getColumnName(1))));
        p.setInfo(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));

        TextView textViewData = (TextView) view.findViewById(R.id.campoDataId);
        TextView textViewInfo = (TextView) view.findViewById(R.id.campoInfoId);

        textViewInfo.setText(p.getInfo());
        textViewData.setText(p.getPrettyData());
    }
}

