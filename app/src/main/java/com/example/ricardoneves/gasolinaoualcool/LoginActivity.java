package com.example.ricardoneves.gasolinaoualcool;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class LoginActivity extends AppCompatActivity {

    private Button entrar;
    private EditText login;
    private EditText senha;
    private EditText confirmSenha;
    private TextView confirmSenha2;
    private TextView novoUsuario;
    private RadioGroup radioGroup;
    private DataUsuario dadosUsu;
    public SQLiteDatabase baseDadosUsu;
    public String loginUsu;
    public String senhaUsu;
    public String confirmSenhaUsu;
    private TextView repitaSenha;

    public Usuario usuario;
    private static final String ARQUIVO_PREFERENCIA = "arquivoPreferencia";
    private Criptografia criptografia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);




        entrar = (Button) findViewById(R.id.entrarId);
        login = (EditText) findViewById(R.id.loginId);
        senha = (EditText) findViewById(R.id.senhaId);
        confirmSenha = (EditText) findViewById(R.id.confirmSenhaId);
        confirmSenha2 = (TextView) findViewById(R.id.repitaSenhaId);
        novoUsuario = (TextView) findViewById(R.id.novoUsuarioID);
        repitaSenha = (TextView) findViewById(R.id.repitaSenhaId);

        loginUsu = new String();
        senhaUsu = new String();
        confirmSenhaUsu = new String();
        criptografia = new Criptografia();

        confirmSenha.setEnabled(false);
        repitaSenha.setEnabled(false);

        try {
            dadosUsu = new DataUsuario(this);
            baseDadosUsu = dadosUsu.getReadableDatabase();

            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setMessage("Banco criado com sucesso");
            build.setNeutralButton("OK", null);
            //build.show();
        } catch (SQLException ex) {
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setMessage("Erro ao criar o banco:" + ex.getMessage());
            build.setNeutralButton("OK", null);
            build.show();
        }

        novoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmSenha.setEnabled(true);
                repitaSenha.setEnabled(true);
                entrar.setText("CADASTRAR");

            }
        });

        /*radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                confirmSenha.setEnabled(true);


            }
        });*/

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                GerenciarUsuario();

            }
        });
    }

    public void GerenciarUsuario() {
        int red = Color.parseColor("#FFFCC0C0");
        int green = Color.parseColor("#94BD31");
        loginUsu = login.getText().toString();
        senhaUsu = senha.getText().toString();
        confirmSenhaUsu = confirmSenha.getText().toString();

        if ((senhaUsu.equals(confirmSenhaUsu)) && (loginUsu.length() > 0)&&(senhaUsu.length() > 0)&&(confirmSenhaUsu.length() > 0))

            {
            if ((checarEntrada(loginUsu, senhaUsu)) == false) {

                try {
                    insertContact(loginUsu, senhaUsu);
                    Toast.makeText(getApplicationContext(), "Usuário Cadastrado", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
       else if (checarEntrada(loginUsu, senhaUsu) == true) {


                Toast.makeText(getApplicationContext(), "Olá" + " " + loginUsu, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));


        } else if (!(senhaUsu.equals(confirmSenhaUsu))&& (loginUsu.length() > 0)&&(senhaUsu.length() > 0)&&(confirmSenhaUsu.length() > 0)){

            Toast.makeText(getApplicationContext(), "Senhas Diferentes", Toast.LENGTH_SHORT).show();
        }
        else if ((loginUsu.length() == 0)&&(senhaUsu.length() > 0)&&(confirmSenhaUsu.length() > 0)){
            confirmSenha.setBackgroundColor(green);
            senha.setBackgroundColor(green);
            login.setBackgroundColor(red);
            Toast.makeText(getApplicationContext(), "Preenchimento Obrigatório", Toast.LENGTH_SHORT).show();
        }

        else if ((loginUsu.length()> 0)&&(senhaUsu.length() == 0)&&(confirmSenhaUsu.length() > 0)){
            confirmSenha.setBackgroundColor(green);
            login.setBackgroundColor(green);
            senha.setBackgroundColor(red);
            Toast.makeText(getApplicationContext(), "Preenchimento Obrigatório", Toast.LENGTH_SHORT).show();
        }
       /* else if ((loginUsu.length()> 0)&&(senhaUsu.length() > 0)&&(confirmSenhaUsu.length() == 0)){
            login.setBackgroundColor(green);
            senha.setBackgroundColor(green);
            confirmSenha.setBackgroundColor(red);
            Toast.makeText(getApplicationContext(), "Preenchimento Obrigatório", Toast.LENGTH_SHORT).show();
        }*/
        else if ((loginUsu.length()> 0)&&(senhaUsu.length() == 0)&&(confirmSenhaUsu.length() == 0)){
            login.setBackgroundColor(green);
            senha.setBackgroundColor(red);
            confirmSenha.setBackgroundColor(red);
            Toast.makeText(getApplicationContext(), "Preenchimento Obrigatório", Toast.LENGTH_SHORT).show();
        }
        else if ((loginUsu.length()== 0)&&(senhaUsu.length() == 0)&&(confirmSenhaUsu.length() == 0)){
            login.setBackgroundColor(red);
            senha.setBackgroundColor(red);
            confirmSenha.setBackgroundColor(red);
            Toast.makeText(getApplicationContext(), "Preenchimento Obrigatório", Toast.LENGTH_SHORT).show();
        }
        else if((loginUsu.length()> 0)&&(senhaUsu.length() > 0)&&(confirmSenhaUsu.length() == 0)){
            Toast.makeText(getApplicationContext(), "Login e/ou Senha Incorretos ", Toast.LENGTH_SHORT).show();
        }
    }



    public boolean insertContact(String l, String s) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("login", l);
        contentValues.put("senha", s);

        baseDadosUsu.insert("Usuario", null, contentValues);
        return true;
    }

    public boolean checarEntrada(String login, String senha) {



        String query = "SELECT * FROM Usuario WHERE TRIM(login) = '"+login.trim()+"' AND TRIM(senha) = '"+senha.trim()+"'";
       // String query = "SELECT * FROM " + "Usuario"+ " WHERE " + "login"+ " = \""+ login +"\" AND "+ "senha" +" = \"" + senha+"\"";


        Cursor cursor = baseDadosUsu.rawQuery(query, null);

        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }

        else  {
            cursor.moveToFirst();
            do {
                criarShared(cursor.getInt(0), login);
            } while (cursor.moveToNext()); }

        cursor.close();
        return true;
    }

    public void criarShared (int id, String login){
        SharedPreferences sharedpreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("id", id);
        editor.putString("login", login);
        editor.commit();
    }
}


