package com.example.ricardoneves.gasolinaoualcool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SobreActivity extends AppCompatActivity {

    private Button back;
    private TextView textoSobre;
    private TextView titulo;
    private String version= "v1.5.1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        back= (Button) findViewById(R.id.voltarId);
        textoSobre= (TextView) findViewById(R.id.sobreId);
        titulo= (TextView) findViewById(R.id.tituloId);

       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent( SobreActivity.this, MainActivity.class));
           }
       });

        titulo.setText("Álcool Ou Gasolina "+ version);
    }
}
