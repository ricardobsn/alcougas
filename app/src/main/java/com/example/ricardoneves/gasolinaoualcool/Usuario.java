package com.example.ricardoneves.gasolinaoualcool;

import java.io.Serializable;

/**
 * Created by ricardo.neves on 21/07/2016.
 */
public class Usuario implements Serializable {

    private String login;
    private int idUsu;

    public Usuario(int idUsu, String login){
        this.setIdUsu(idUsu);
        this.setLogin(login);

    }
    public Usuario(){

    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getIdUsu() {
        return idUsu;
    }

    public void setIdUsu(int idUsu) {
        this.idUsu = idUsu;
    }
}
