package com.example.ricardoneves.gasolinaoualcool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ricardo.neves on 20/07/2016.
 */
public class DataUsuario  extends SQLiteOpenHelper {


    public DataUsuario(Context context) {
        super(context, "Usuario", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(getCreateUsuario());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    public static String getCreateUsuario(){


        StringBuilder build= new StringBuilder();

        build.append("CREATE TABLE IF NOT EXISTS Usuario ( ");
        build.append("_id_Usu            INTEGER             NOT NULL ");
        build.append("PRIMARY KEY AUTOINCREMENT, ");
        build.append("login             VARCHAR (10), ");
        build.append("senha             VARCHAR (10) ");
        build.append("); ");

        return build.toString();
    }



}
