package com.example.ricardoneves.gasolinaoualcool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SegundaActivity extends Activity {

    private Button voltar;
    private ListView scroll;
    private ArrayList logDeCombustivel;
    public SQLiteDatabase bancoDados;
    private ArrayList<Integer> ids;
    public SQLiteDatabase aux2;
    private DataBase bandados;
    private static final String ARQUIVO_PREFERENCIA = "arquivoPreferencia";
    private int idUsu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logDeCombustivel= new ArrayList<String>();
        ids= new ArrayList<Integer>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        voltar = (Button) findViewById(R.id.voltarId);
        scroll = (ListView) findViewById(R.id.listaId);



        bandados = new DataBase(this);

        aux2 = bandados.getReadableDatabase();

        visualizarBanco();

        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SegundaActivity.this, MainActivity.class));

            }
        });


    }


    public void visualizarBanco(){
        try {
            Cursor cursor = aux2.query("LogComb", null, "trackUsuario ="+ getUserId() , null, null, null, null);

            TodoCursorAdapter cursorAdapter = new TodoCursorAdapter(getApplicationContext(), cursor);

            scroll.setAdapter(cursorAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getUserId() {

        SharedPreferences sharedpreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, Context.MODE_PRIVATE);
        if (sharedpreferences.contains("id")){
             idUsu= sharedpreferences.getInt("id",0);
        }
        return idUsu;
    }


}
